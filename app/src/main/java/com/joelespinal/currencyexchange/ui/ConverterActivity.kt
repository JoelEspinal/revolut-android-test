package com.joelespinal.currencyexchange.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.joelespinal.currencyexchange.R
import com.joelespinal.currencyexchange.databinding.ActivityMainBinding
import com.joelespinal.currencyexchange.models.Rate
import com.joelespinal.currencyexchange.ui.adapters.CurrencyAdapter
import com.joelespinal.currencyexchange.viewModel.ConverterViewModel
import java.util.*

class ConverterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var currencyAdapter: CurrencyAdapter
    private lateinit var currencyLayoutManager: RecyclerView.LayoutManager

    private lateinit var converterViewModel: ConverterViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setupRecycleView()
    }

    private fun setupRecycleView() {
        val currencyRecyclerView = binding.currencyRecycleView
        currencyLayoutManager = LinearLayoutManager(this)
        currencyRecyclerView.layoutManager = currencyLayoutManager



        currencyAdapter = CurrencyAdapter(this, Collections.emptyList())
        currencyRecyclerView.adapter = currencyAdapter
        converterViewModel = ViewModelProviders.of(this).get(ConverterViewModel::class.java)
        converterViewModel.getCurrencies().observe(this, Observer {
            currencyAdapter.setCurrencies((it as MutableList<Rate>))

        })
    }
}