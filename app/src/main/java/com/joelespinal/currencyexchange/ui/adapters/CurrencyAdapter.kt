package com.joelespinal.currencyexchange.ui.adapters

import android.content.Context
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import com.joelespinal.currencyexchange.databinding.CurrencyItemBinding
import com.joelespinal.currencyexchange.models.Rate
import com.joelespinal.currencyexchange.utils.NameUtil
import java.text.DecimalFormat


class CurrencyAdapter(context: Context, private var rates: MutableList<Rate>) :
    RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder>() {

    private val context = context
    private lateinit var recyclerView: RecyclerView
    private var currentValue = 1.0
    private var onBind = true

    inner class CurrencyViewHolder(var binding: CurrencyItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(rate: Rate) {
            binding.rate = rate
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = CurrencyItemBinding.inflate(layoutInflater, parent, false)
        return CurrencyViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return rates.size
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        val currency = rates[position]

        holder.binding.nameTextView.text = NameUtil.getComertialName(currency.baseName)

        var imageName: String
        imageName = if (currency.baseName == "TRY") {
            "_try"
        } else {
            currency.baseName
        }

        val currencyIcon = this.context.resources.getIdentifier(
            "${imageName.toLowerCase()}_round",
            "mipmap",
            context.packageName
        )

        holder.binding.logoImageView.setImageResource(currencyIcon)
        onBind = false
        if (position == 0) {
            holder.binding.valueTextView.isEnabled = true
            holder.binding.valueTextView.placeCursorToEnd()
            holder.binding.valueTextView.doOnTextChanged { text, start, count, after ->
                if (text.toString().trim().isNotEmpty()) {
                    this.currentValue = text.toString().toDouble()
                }
            }

            holder.binding.currencyLayout.hasOnClickListeners()
        } else {
            holder.binding.valueTextView.isEnabled = false

            val formatter = DecimalFormat(".##")
            val formattedValue = formatter.format((currency.value * currentValue))
            holder.binding.valueTextView.setText(formattedValue)
        }

        holder.binding.currencyLayout.setOnClickListener {
            swapToFirst(holder.adapterPosition)
        }

        holder.bind(currency)
        onBind = true
    }

    private fun updateCurrencies() {
        if (onBind) {
            notifyItemRangeChanged(1, rates.size)
        }


    }

    fun setCurrencies(rates: MutableList<Rate>) {
        if (this.rates.size > 0) {
            this.rates.forEach { rt ->
                rates.forEach {
                    if (rt.baseName == it.baseName) {
                        rt.value = currentValue * it.value
                    }
                }
            }
        } else {
            this.rates = rates
        }

        updateCurrencies()
    }

    private fun swapToFirst(position: Int) {
        if (position > 0) {
            var selectedRate = rates.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRemoved(0)
            rates.add(0, selectedRate)
            notifyItemInserted(0)
            notifyItemRangeChanged(1, position)
        }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
        (this.recyclerView.itemAnimator as DefaultItemAnimator).supportsChangeAnimations = false
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    private fun EditText.placeCursorToEnd() = this.setSelection(this.text.length, this.text.length)
}