package com.joelespinal.currencyexchange.utils

class NameUtil {
    companion object {
        fun getComertialName(abbreviation: String): String = when (abbreviation) {
            "EUR" -> "EURO"
            "THB" -> "Thai Baht"
            "JPY" -> "Japanese Yen"
            "CZK" -> "Czech Koruna"
            "USD" -> "US DOLLAR"
            "AUD" -> "Australian Dollar"
            "BRL" -> "Brazilian Real"
            "BGN" -> "Bulgarian Lev"
            "CAD" -> "Canadian Dollar"
            "CHF" -> "Swiss Franc"
            "CNY" -> "Chinese Yuan"
            "DKK" -> "Danish Krone"
            "GBP" -> "Pound Sterling"
            "HKD" -> "Hong Kong Dollar"
            "HRK" -> "Croatian Kuna"
            "HUF" -> "Hungary Forints"
            "IDR" -> "Indonesian rupiah"
            "ILS" -> "Israeli Shekel"
            "INR" -> "Indian Rupee"
            "ISK" -> "Icelandic Crown"
            "KRW" -> "South Korean won"
            "MXN" -> "Mexican Peso"
            "MYR" -> "Malaysian ringgit"
            "NOK" -> "Norway Kroner"
            "NZD" -> "New Zealand dollar"
            "PHP" -> "Philippine peso"
            "PLN" -> "Poland Zloty"
            "RON" -> "Romanian Leu"
            "SEK" -> "Swedish Krona"
            "SGD" -> "Singapore dollar"
            "THB" -> "Thai Baht"
            "TRY" -> "Turkish Lira"
            "ZAR" -> "South African Rand"
            "RUB" -> "Russian Ruble"
            else -> ""
        }
    }
}