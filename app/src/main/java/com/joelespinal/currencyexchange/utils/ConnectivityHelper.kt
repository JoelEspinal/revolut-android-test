package com.joelespinal.currencyexchange.utils

import android.content.Context
import android.net.ConnectivityManager
import com.joelespinal.currencyexchange.main.App


class ConnectivityHelper private constructor() {

    companion object {
        private val connectivityManager = App.getContext()
            ?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?

        fun isConnected(): Boolean {
            val netInfo = connectivityManager!!.activeNetworkInfo
            return netInfo != null && netInfo.isConnectedOrConnecting
        }
    }
}