package com.joelespinal.currencyexchange.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.joelespinal.currencyexchange.models.Rate
import com.joelespinal.currencyexchange.remote.Retrofit
import com.joelespinal.currencyexchange.remote.api.RateApi
import com.joelespinal.currencyexchange.utils.ConnectivityHelper
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.launch
import java.net.HttpURLConnection

class RateRepository {

    private val rateService: RateApi = Retrofit.instance.create(RateApi::class.java)
    private val rateData = MutableLiveData<List<Rate>>()

    fun getRates(baseCurrency: String = "EUR"): MutableLiveData<List<Rate>> {

        val tickerChannel = ticker(delayMillis = 1000, initialDelayMillis = 0)

        GlobalScope.launch {
            for (event in tickerChannel) {
                if (ConnectivityHelper.isConnected()) {
                    try {
                        var response = rateService.getRates(baseCurrency)
                        if (response.isSuccessful && response.body() != null) {
                            val currency = response.body()
                            rateData.postValue(currency!!.convert().rates)
                        } else {
                            var errorMessage = when (response.code()) {
                                HttpURLConnection.HTTP_NOT_FOUND -> "ups, Service not found"
                                HttpURLConnection.HTTP_CLIENT_TIMEOUT -> "Time out error"
                                else -> "Unknown error has occurs"
                            }


//                            Log.d("CALL_ERROR", errorMessage)
                        }
                    } catch (e: Exception) {
//                        Log.d("ERROR_CALL", e.message)
                    }
                }
            }
        }

        return rateData
    }
}