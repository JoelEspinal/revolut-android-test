package com.joelespinal.currencyexchange.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.joelespinal.currencyexchange.models.Rate
import com.joelespinal.currencyexchange.repository.RateRepository

class ConverterViewModel : ViewModel() {

    private val rateRepository = RateRepository()

    fun getCurrencies(): MutableLiveData<List<Rate>> {
        return rateRepository.getRates()
    }
}