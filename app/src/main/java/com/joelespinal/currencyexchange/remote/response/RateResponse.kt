package com.joelespinal.currencyexchange.remote.response

import com.google.gson.annotations.SerializedName
import com.joelespinal.currencyexchange.models.Currency
import com.joelespinal.currencyexchange.models.Rate

class RateResponse {
    @SerializedName("base")
    var base: String? = null
    @SerializedName("rates")
    var rates: Map<String, Double>? = null

    fun convert(): Currency {

        var rateList: MutableList<Rate> = ArrayList()

        this.rates?.map {
            rateList.add(Rate(baseName = it.key, value = it.value))
        }



        return Currency(this.base, (rateList as List<Rate>))
    }
}

