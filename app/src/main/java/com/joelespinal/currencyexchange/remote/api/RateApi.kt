package com.joelespinal.currencyexchange.remote.api

import com.joelespinal.currencyexchange.remote.response.RateResponse
import io.reactivex.Flowable
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface RateApi {

    @Headers("Content-Type: application/json")
    @GET("/latest")
   suspend fun getRates(@Query("base") base: String): Response<RateResponse>
}