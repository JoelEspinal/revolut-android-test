package com.joelespinal.currencyexchange.main

import android.app.Application
import android.content.Context

class App : Application() {

    companion object {
        private lateinit var appContext: Context

        fun setContext(context: Context) {
            this.appContext = context
        }

        fun getContext(): Context = appContext
    }

    override fun onCreate() {
        super.onCreate()
        setContext(applicationContext)
    }
}