package com.joelespinal.currencyexchange.models

data class Currency(val base: String?, val rates: List<Rate>)