package com.joelespinal.currencyexchange.models

data class Rate(
    var baseName: String,
    var value: Double = 1.0
)
